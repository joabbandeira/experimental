<!DOCTYPE html>
<html>
    <head>
        <title>Projeto Experimental</title>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <link href="https://fonts.googleapis.com/css?family=Lato:100|Gafata" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" />
        <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('css/clock.css') }}" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <header></header>
        <div class="content">
            <div class="row nomg">
                <div class="col-md-6 col-sm-12 col-xs-12 left" style="background-image:url('<?= $weather['bg'] ?>');">

                    <div class="weather">
                        <h2>{{$weather['city']}}</h2>
                        <img src="{{$weather['img']}}" />
                        <div class="temp">{{$weather['temp']}}</div>
                        <div class="img-temp">{{$weather['img_temp']}}</div>
                        <div class="desc">{{$weather['desc']}}</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 right">
                    <center>
                        <h2>Panorama COMERC</h2>
                    </center>
                    <div id="contPanorama">
                        <ul>
                            <?php foreach ($posts->channel->item as $entry) { ?>
                                <li>
                                    <a href="{{$entry->link}}" title="{{$entry->title}}" target="_blank"> {{$entry->title}}</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div id="clock" class="light">
                <div class="display">
                    <div class="weekdays"></div>
                    <div class="ampm"></div>
                    <div class="alarm"></div>
                    <div class="digits"></div>
                </div>
            </div>
        </footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="<?php echo asset('js/app.js'); ?>"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/moment.min.js"></script>
        <script src="<?php echo asset('js/clock.js'); ?>"></script>
        <script>

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var time = 60 * 1000;
            $(function poll() {
                setTimeout(function () {
                    //Atualiza listagem de posts
                    $.ajax({
                        url: '{{url()->current()}}/getPosts',
                        type: "GET",
                        data: "",
                        success: function (response) {
                            var response = JSON.parse(response);
                            var html = '<ul>';
                            $.each(response, function (index, value) {
                                html += '<li><a href="' + response[index].link + '" title="' + response[index].title + '" target="_blank">' + response[index].title + '</a></li>';
                            });
                            html += '</ul>';
                            $('#contPanorama').html(html);
                        },
                        complete: poll
                    });

                    //Atualiza clima
                    $.ajax({
                        url: '{{url()->current()}}/getWeather',
                        type: "GET",
                        data: "",
                        success: function (response) {
                            var response = JSON.parse(response);
                            $('div.left').css({
                                backgroundImage: 'url("' + response.bg + '")'
                            });
                            $('div.weather').find('h2').html(response.city);
                            $('div.weather').find('img').attr('src', response.img);
                            $('div.weather').find('div.temp').html(response.temp);
                            $('div.weather').find('div.img-temp').html(response.img_temp);
                            $('div.weather').find('div.desc').html(response.desc);
                        },
                        complete: poll
                    });
                }, time);
            });
        </script>
    </body>
</html>
