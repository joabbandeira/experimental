<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use SimpleXMLElement;
use Goutte\Client;

class RssController extends Controller {

    public function showRss() {

        $posts = $this->getPosts();

        $weather = $this->getWeather();

        $vars = [
            'posts' => $posts,
            'weather' => $weather
        ];

        return view('rss', $vars);
    }

    public function getPosts() {

        $getPosts = trim(file_get_contents('http://www.panoramacomerc.com.br/?feed=rss2'));
        $posts = new \SimpleXMLElement($getPosts);

        return $posts;
    }

    public function getWeather($location = 'sp-sao-paulo') {

        $client = new Client();

        $url = "http://climaagora.com.br/clima-agora/" . $location;
        $crawler = $client->request("GET", $url);

        $getBg = $crawler->filter('div[id="page"]')->attr('style');

        $weather['bg'] = 'http://www.climaagora.com.br' . str_replace([')', ';'], '', explode('(', $getBg))[1];
        $weather['city'] = $crawler->filter('div[id="clima-atual"]')->filter('div[class="cidade"]')->text();
        $weather['img'] = 'http://www.climaagora.com.br' . $crawler->filter('div[id="clima-atual"]')->filter('img[class="left"]')->attr('src');
        $weather['temp'] = $crawler->filter('div[id="clima-atual"]')->filter('div[class="left"]')->filter('div[class="temp"]')->html();
        $weather['img_temp'] = $crawler->filter('div[id="clima-atual"]')->filter('div[class="left"]')->filter('div[class="img-temp"]')->text();
        $weather['desc'] = $crawler->filter('div[id="clima-atual"]')->filter('div[class="left"]')->filter('div[class="desc"]')->html();
        $weather['time'] = $crawler->filter('div[id="clima-atual"]')->filter('div[class="atualizacao"]')->text();

        return $weather;
    }

    public function getPostsAjax() {

        $posts = $this->getPosts();

        $i = 0;
        foreach ($posts->channel->item as $entry) {

            $ar = (array) $entry;

            $post[$i]['title'] = $ar['title'];
            $post[$i]['link'] = $ar['link'];
            $i++;
        }

        return json_encode($post);
    }

    public function getWeatherAjax() {

        $weather = $this->getWeather();

        return json_encode($weather);
    }

}
